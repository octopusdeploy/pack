# Bitbucket Pipelines Pipe: Octo Pack

Creates a package (.nupkg or .zip) from files on disk, without needing a .nuspec or .csproj

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: octopusdeploy/pack:0.6.1
  variables:
    ID: "<string>"
    FORMAT: "<string>"
    VERSION: "<string>"
    SOURCE_PATH: "<string>"
    OUTPUT_PATH: "<string>"
    # EXTRA_ARGS: ['<string>','<string>' ..] # Optional
    # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| ID (*)              | The ID of the package; e.g. `MyCompany.MyApp` |
| FORMAT (*)              | Package format; Options are: `Zip`, `NuPkg` |
| VERSION (*)              | The version of the package; must be a valid Semantic Version; |
| SOURCE_PATH (*)              | The root folder containing files and folders to pack; Default: `'.'` |
| OUTPUT_PATH (*)              | The folder into which the generated package file will be written; Default: `./out`. |
| EXTRA_ARGS                 | Include any additional command line arguments to the `pack` command. |
| DEBUG                 | Turn on extra debug information; Default: `false`. |

_(*) = required variable._

## Prerequisites

A folder (specified by the variable `$SOURCE_PATH`) which contains any files to be packaged. 

## Task Output
The pipe will create a package file based on the supplied variables in the format `$ID.$VERSION.$FORMAT`. This file will be available in the directory specified by the `$OUTPUT_PATH` variable. To use the generated package in additional steps, you can make use of Bitbucket [artifacts](https://confluence.atlassian.com/bitbucket/using-artifacts-in-steps-935389074.html).

**Note:** If packaging multiple files, you should specify either:
 - a unique folder per run of the pipe, or
 - try to use the root directory to save your package files

This is due to the way Bitbucket pipelines run in containers, meaning once a folder has been created by an initial run of the pipe, subsequent runs are unable to write to it.

### Documentation

Refer to the following documentation for more detail:

* [Octopus CLI pack command](https://octopus.com/docs/octopus-rest-api/octopus-cli/pack)

## Examples

Basic example:

```yaml
script:
  - pipe: octopusdeploy/pack:0.6.1
    variables:
      ID: $PACKAGE_ID
      FORMAT: $PACKAGE_FORMAT
      VERSION: $PACKAGE_VERSION
      SOURCE_PATH: '.'
      OUTPUT_PATH: './output'
artifacts:
  - output/*.zip
```

Advanced example:

```yaml
script:
  - pipe: octopusdeploy/pack:0.6.1
    variables:
      ID: $PACKAGE_ID
      FORMAT: $PACKAGE_FORMAT
      VERSION: $PACKAGE_VERSION
      SOURCE_PATH: '.'
      OUTPUT_PATH: './output'
      DEBUG: "true"
      EXTRA_ARGS: ['--compressionlevel', 'fast']
artifacts:
  - output/*.zip
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by support@octopus.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
