# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.6.1

- patch: Correct EXTRA_ARGS yaml def and add note about multiple pipe runs

## 0.6.0

- minor: Updating octo image

## 0.5.1

- patch: Remove whitespace from yaml def in readme

## 0.5.0

- minor: Removed script from pipe yaml in README

## 0.4.1

- patch: Create new tagged version where README.md pipe matches Docker image name

## 0.4.0

- minor: Support only one output from pack
- patch: Continue to try permissions check for output_path

## 0.3.1

- patch: Tweak permissions check for output_path

## 0.3.0

- minor: Added creation of output dir if not exists

## 0.2.0

- minor: Remove variable declaration from pipe.yml

## 0.1.0

- minor: Initial release of Bitbucket Pipelines Octopus pack pipe.

